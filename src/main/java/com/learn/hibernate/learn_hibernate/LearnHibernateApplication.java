package com.learn.hibernate.learn_hibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.scheduling.annotation.EnableAsync;

import static org.springframework.hateoas.config.EnableHypermediaSupport.HypermediaType.HAL;

@SpringBootApplication
@EnableAsync
@EnableHypermediaSupport(type = HAL)
public class LearnHibernateApplication
{

    public static void main(String[] args)
    {
        SpringApplication.run(LearnHibernateApplication.class, args);

    }
}
