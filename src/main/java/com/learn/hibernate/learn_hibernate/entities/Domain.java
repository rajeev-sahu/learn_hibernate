package com.learn.hibernate.learn_hibernate.entities;

import javax.persistence.*;

@Entity
public class Domain extends BasicEntity
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(columnDefinition = " TEXT NOT NULL UNIQUE")
    private String domainName;

    @Column(name = "user_id", unique = true, nullable = false)
    private Long userId;


    @Column(columnDefinition = " TEXT NOT NULL UNIQUE")
    private String poolName;

    @Column(columnDefinition = " TEXT NOT NULL UNIQUE")
    private String status;

    public Domain()
    {
    }

    public Long getId()
    {
        return id;
    }

    public String getDomainName()
    {
        return domainName;
    }

    public Long getUserId()
    {
        return userId;
    }

    public String getPoolName()
    {
        return poolName;
    }

    public String getStatus()
    {
        return status;
    }
}
