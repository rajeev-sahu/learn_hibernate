package com.learn.hibernate.learn_hibernate.entities;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Passport extends BasicEntity
{
    @Id
    @GeneratedValue
    private Long id;

    private String number;

    @OneToOne(mappedBy = "passport")
    private Student student;

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
                .append("id", id)
                .append("number", number)
                .toString();
    }

    public Passport(final String number)
    {
        this.number = number;
    }

    public Passport()
    {

    }

    public Long getId()
    {
        return id;
    }

    public String getNumber()
    {
        return number;
    }
}
