package com.learn.hibernate.learn_hibernate.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Student extends BasicEntity
{
    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @OneToOne(fetch = FetchType.EAGER)
    private Passport passport;

    @ManyToMany
    @JoinTable(name = "student_courses", joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "course_id"))

    private List<Course> courses = new ArrayList<>();

    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
                .append("id", id)
                .append("name", name)
                .toString();
    }

    public Student(final String name, final Passport passport)
    {

        this.name = name;
        this.passport = passport;
    }

    public Student()
    {

    }

    public Passport getPassport()
    {

        return passport;
    }

    public List<Course> getCourses()
    {
        return new ArrayList<>(courses);
    }

    public Long getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }
}
