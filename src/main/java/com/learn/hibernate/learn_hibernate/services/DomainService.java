package com.learn.hibernate.learn_hibernate.services;

import com.learn.hibernate.learn_hibernate.jpa.DomainRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
public class DomainService {

    private final DomainRepository domainRepository;

    private final DummyDomainDService dummyDomainDService;

    @Autowired
    public DomainService(final DomainRepository domainRepository,
                         DummyDomainDService dummyDomainDService) {

        this.domainRepository = domainRepository;
        this.dummyDomainDService = dummyDomainDService;
    }

    //    @PreAuthorize("hasPermission(#domainName,'')")
    @PreAuthorize("isDomainOfCurrentUser(#domainName)")
    public String getPoolName(final DomainName domainName) {

        System.out.println("Entering getPoolName");

        dummyDomainDService.getPoolName(domainName.getDomainName());

        publicMethod(domainName.getDomainName());
        privateMethod(domainName.getDomainName());

        return domainRepository.findByDomainName(domainName.getDomainName())
                               .orElseThrow(RuntimeException::new)
                               .getPoolName();
    }

    @PreAuthorize("isDomainOfCurrentUser(#domainName)")
    private void privateMethod(final String domainName) {

        System.out.println("Private method");
    }

    @PreAuthorize("isDomainOfCurrentUser(#domainName)")
    private void publicMethod(final String domainName) {

        System.out.println("Public method");
    }
}
