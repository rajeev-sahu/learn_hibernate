package com.learn.hibernate.learn_hibernate.services;

import com.learn.hibernate.learn_hibernate.entities.Course;
import com.learn.hibernate.learn_hibernate.entities.Student;
import com.learn.hibernate.learn_hibernate.jpa.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService
{
    private final StudentRepository repository;

    @Autowired
    public StudentService(final StudentRepository repository)
    {
        this.repository = repository;
    }

//    @PreFilter("hasPermission(targetObject, 'delete')")
    public List<Course> getCourses(final Long studentId)
    {
        final Optional<Student> optionalStudent = repository.findById(studentId);

        final Student student = optionalStudent.orElseThrow(RuntimeException::new);

        return student.getCourses();
    }
}
