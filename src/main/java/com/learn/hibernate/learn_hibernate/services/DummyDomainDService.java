package com.learn.hibernate.learn_hibernate.services;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
public class DummyDomainDService
{
    @PreAuthorize("isDomainOfCurrentUser(#domainName)")
    public String getPoolName(final String domainName)
    {
        System.out.println("Entering DummyDomainDService.getPoolName");
        return "Done";
    }

}
