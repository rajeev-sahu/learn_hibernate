package com.learn.hibernate.learn_hibernate.services;

import com.learn.hibernate.learn_hibernate.jpa.DomainRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthService {
    private final DomainRepository domainRepository;

    @Autowired
    public AuthService(final DomainRepository domainRepository) {

        this.domainRepository = domainRepository;
    }

    public void authenticateOrderOfUser(final Long userId,
                                        final DomainName domainName) {

        if (domainRepository.findByDomainNameAndUserIdAndStatusNot(domainName.getDomainName(),userId,"Deleted" ).isEmpty()) {

            throw new RuntimeException();
        }
    }

}
