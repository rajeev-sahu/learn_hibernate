package com.learn.hibernate.learn_hibernate.services;

public class DomainName {

    private final String domainName;

    public DomainName(final String domainName) {

        this.domainName = domainName;
    }

    public String getDomainName() {

        return domainName;
    }
}
