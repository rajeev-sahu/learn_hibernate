package com.learn.hibernate.learn_hibernate.jpa;

import com.learn.hibernate.learn_hibernate.entities.Domain;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DomainRepository extends JpaRepository<Domain, Long>
{
    Optional<Domain> findByDomainName(final String domainName);

    List<Domain> findByDomainNameAndUserIdAndStatusNot(final String domainName,
                                                       final Long userId,
                                                       final String status);


}
