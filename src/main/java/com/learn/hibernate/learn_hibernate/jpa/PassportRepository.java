package com.learn.hibernate.learn_hibernate.jpa;

import com.learn.hibernate.learn_hibernate.entities.Passport;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PassportRepository extends JpaRepository<Passport, Long>
{

}
