package com.learn.hibernate.learn_hibernate.jpa;

import com.learn.hibernate.learn_hibernate.entities.Course;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CourseRepository extends JpaRepository<Course,Long>
{
    public Optional<Course> findById(final Long id);
}
