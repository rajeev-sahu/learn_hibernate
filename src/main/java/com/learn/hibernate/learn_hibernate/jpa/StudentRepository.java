package com.learn.hibernate.learn_hibernate.jpa;

import com.learn.hibernate.learn_hibernate.entities.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student, Long>
{
    List<Student> findByName(final String name);
}
