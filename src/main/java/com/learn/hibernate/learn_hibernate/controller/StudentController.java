package com.learn.hibernate.learn_hibernate.controller;

import com.learn.hibernate.learn_hibernate.entities.Course;
import com.learn.hibernate.learn_hibernate.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = "/students")
public class StudentController
{
    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService)
    {
        this.studentService = studentService;
    }

    @GetMapping("{studentId}")
    public List<Course> getCourses(@PathVariable(name = "studentId") final Long studentId)
    {
        List<Course> courses = studentService.getCourses(studentId);
        List<Course> returnList = new ArrayList<>();
        for (Course course : courses)
        {
            System.out.println(course);
            returnList.add(course);
        }
        return returnList;
    }
}
