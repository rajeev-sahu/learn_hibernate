package com.learn.hibernate.learn_hibernate.controller;

import com.learn.hibernate.learn_hibernate.services.DomainName;
import com.learn.hibernate.learn_hibernate.services.DomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/domains")
public class DomainController {

    private final DomainService domainService;

    @Autowired
    public DomainController(DomainService domainService) {

        this.domainService = domainService;
    }

    @GetMapping("{domainName:.+}")
    public String getCourses(@PathVariable(name = "domainName") final String domainName,
                             @RequestHeader(name = "user-id") final Long userId) {

        return domainService.getPoolName(new DomainName(domainName));
    }
}
