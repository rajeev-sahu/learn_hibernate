package com.learn.hibernate.learn_hibernate.config;

import com.learn.hibernate.learn_hibernate.services.AuthService;
import com.learn.hibernate.learn_hibernate.services.DomainName;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;


public class CoreMethodSecurityExpressionRoot extends SecurityExpressionRoot implements MethodSecurityExpressionOperations
{
    private final AuthService authService;

    CoreMethodSecurityExpressionRoot(final Authentication authentication,
                                     final AuthService authService)
    {
        super(authentication);
        this.authService = authService;
    }

    @Override
    public void setFilterObject(Object o)
    {

    }

    @Override
    public Object getFilterObject()
    {
        return null;
    }

    @Override
    public void setReturnObject(Object o)
    {

    }

    @Override
    public Object getReturnObject()
    {
        return null;
    }

    @Override
    public Object getThis()
    {
        return null;
    }

    public boolean isDomainOfCurrentUser(final DomainName domainName)
    {
        System.out.println("Entering isDomainOfCurrentUser");

        final String userId = ((User) authentication.getPrincipal()).getUsername();

        authService.authenticateOrderOfUser(Long.parseLong(userId), domainName);

        return true;
    }
}
