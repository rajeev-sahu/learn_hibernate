package com.learn.hibernate.learn_hibernate.config;

import com.learn.hibernate.learn_hibernate.services.AuthService;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
public class CoreMethodSecurityExpressionHandler extends DefaultMethodSecurityExpressionHandler
{
    private final AuthService authService;

    private AuthenticationTrustResolver trustResolver = new AuthenticationTrustResolverImpl();

    public CoreMethodSecurityExpressionHandler(final AuthService authService)
    {
        this.authService = authService;
    }

    @Override
    protected MethodSecurityExpressionOperations createSecurityExpressionRoot(Authentication authentication,
                                                                              MethodInvocation invocation)
    {
        CoreMethodSecurityExpressionRoot root = new CoreMethodSecurityExpressionRoot(authentication,
                                                                                     authService);
        root.setPermissionEvaluator(getPermissionEvaluator());
        root.setTrustResolver(this.trustResolver);
        root.setRoleHierarchy(getRoleHierarchy());
        return root;
    }
}

