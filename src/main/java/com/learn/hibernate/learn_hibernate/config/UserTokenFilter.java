package com.learn.hibernate.learn_hibernate.config;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Collections;

public class UserTokenFilter extends GenericFilterBean {

    @Override
    public void doFilter(ServletRequest request,
                                                   ServletResponse response,
                                                   FilterChain chain) throws ServletException, IOException {

        final String userId = ((HttpServletRequest) request).getHeader("user-id");

        if (userId == null) {
            chain.doFilter(request, response);
            return;
        }

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(new User(userId,
                                                                                                              "",
                                                                                                              Collections.emptyList()),
                                                                                                     null));
        chain.doFilter(request, response);
    }
}
